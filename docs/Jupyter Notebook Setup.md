Pre Reqs
=================

- Setup from the README
- VS Code - Recommended


Follow these Instuctions 
=================

1. Using git clone this repo with ssh `git clone git@gitlab.com:tufinps/pytos2-ce.git` (recommended vs https)

*Recommended Method*
[https://code.visualstudio.com/docs/datascience/jupyter-notebooks](Jupyter Notebooks in VS Code)

2. Now edit the `.env` file with the variables of your lab

3. You should now be able to use our notebooks

> Note: if you encounter an error that says something wasnt installed after you ran pip install - restart the noetbook kernal